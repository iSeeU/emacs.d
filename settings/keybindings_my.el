;; Custom keybindings

;; Buffer Selection and ibuffer settings
(require 'bs)
(require 'ibuffer)
(global-set-key (kbd "<f2>") 'bs-show) ;; запуск buffer selection кнопкой F2

;; Delete trailing whitespaces, format buffer and untabify when save buffer
(defun format-current-buffer()
    (indent-region (point-min) (point-max)))
(defun untabify-current-buffer()
    (if (not indent-tabs-mode)
        (untabify (point-min) (point-max)))
    nil)

(add-to-list 'write-file-functions 'format-current-buffer)
(add-to-list 'write-file-functions 'untabify-current-buffer)
(add-to-list 'write-file-functions 'delete-trailing-whitespace)

;;(global-set-key (kbd "C-x C-b") 'electric-buffer-list)
;;(key-chord-define-global "bb" 'electric-buffer-list)


;; Multiple cursors
;;(global-set-key (kbd "C-c <right>") 'mc/mark-next-like-this-word) ; choose same word next
;;(global-set-key (kbd "C-c <left>") 'mc/mark-previous-word-like-this) ; choose same word previous
;;(global-set-key (kbd "M-n") 'mc/mark-next-like-this) ; choose char from next line same position
;;(global-set-key (kbd "M-m") 'mc/mark-previous-like-this); choose char from previous line same position

;;(global-set-key (kbd "C-c C-_") 'mc/mark-all-like-this)
;;(global-set-key (kbd "C-x M-m") 'back-to-indentation)


;; Smartparent
;;(global-unset-key (kbd "C-c w"))
;;(global-set-key (kbd "C-c w") 'sp-rewrap-sexp)
;;(key-chord-define-global "''" 'sp-rewrap-sexp)

;; Undo-tree
;;(global-unset-key (kbd "C-z"))


;; Expand region
(global-unset-key (kbd "C-q"))
(global-set-key (kbd "C-q") 'er/expand-region)
(key-chord-define-global "//" 'er/expand-region)

;; delete line
(defun my-delete-line ()
  "Delete text from current position to end of line char."
  (interactive)
  (kill-region
   (move-beginning-of-line 1)
   (save-excursion (move-end-of-line 1) (point)))
  (delete-char 1)
)
(global-set-key (kbd "C-d") 'my-delete-line)

(global-set-key (kbd "RET") 'newline-and-indent) ;; при нажатии Enter перевести каретку и сделать отступ

(defvar newline-and-indent t)

;; tab indent or complete
;;(defun check-expansion ()
;;  (save-excursion
;;    (if (looking-at "\\_>") t
;;      (backward-char 1)
;;      (if (looking-at "\\.") t
;;        (backward-char 1)
;;        (if (looking-at "->") t nil)))))

;;(defun do-yas-expand ()
;;  (let ((yas/fallback-behavior 'return-nil))
;;    (yas/expand)))

;;(defun tab-indent-or-complete ()
;;  (interactive)
;;  (message (minibufferp))
;;  (if (minibufferp)
;;      (minibuffer-complete)
;;    (if (or (not yas/minor-mode)
;;            (null (do-yas-expand)))
;;        (if (check-expansion)
;;            (company-complete-common)
;;          (indent-for-tab-command)))))

;;(global-set-key [tab] 'tab-indent-or-complete)


(key-chord-define-global "xx" 'save-buffer)
(key-chord-define-global "qq" 'delete-other-windows)
(key-chord-define-global "vv" 'save-buffers-kill-terminal)


(provide 'keybindings_my)
